# Examples

included are two ansible playbooks used to create a ceph cluster.


`install-etcd.yml`: creates the database as well as the user accounts and puts the etcd files in the correct location across all hosts.

`install-rally.yml`: does everything else related to installing a ceph cluster.
