---

- name: copy host file to node
  template:
    src: templates/common_hosts.j2
    dest: /etc/hosts
    owner: root
    group: root
    mode: 0644
  become: yes

- name: Create a wheel group
  group:
    name: wheel
    state: present
  become: yes

- name: Allow wheel group to have passwordless sudo
  lineinfile:
    dest: /etc/sudoers
    state: present
    regexp: '^%wheel'
    line: '%wheel ALL=(ALL) NOPASSWD: ALL'
  become: yes

- name: "Create {{ ceph_user }} user"
  user:
    name: "{{ ceph_user }}"
    comment: Ceph Client
    create_home: yes
    shell: /bin/bash
    groups: wheel
    append: yes
    generate_ssh_key: yes
    ssh_key_bits: 1024
    ssh_key_file: .ssh/id_rsa
    state: present
  become: yes

- name: copying over public key as authorized user
  copy:
    src: files/common_keys/ceph_admin.pub
    dest: "/home/{{ ceph_user }}/.ssh/authorized_keys"
    force: yes
    owner: "{{ ceph_user }}"
    mode: 0644
  become: yes

- name: "copying over private key to {{ ceph_user }}"
  copy:
    src: files/common_keys/ceph_admin
    dest: "/home/{{ ceph_user }}/.ssh/ceph_admin"
    force: yes
    owner: "{{ ceph_user }}"
    mode: 0700
  become: yes

- name: copy ssh config file to client
  template:
    src: templates/common_ssh_config.j2
    dest: "/home/{{ ceph_user }}/.ssh/config"
    owner: "{{ ceph_user }}"
    group: "{{ ceph_user }}"
    mode: 0700
  become: yes

- name: wipe known hosts
  copy:
    content: ""
    dest: "/home/{{ ceph_user }}/.ssh/known_hosts"
    force: yes
    owner: "{{ ceph_user }}"
    group: "{{ ceph_user }}"
    mode: 0644
  become: yes


# this is more of an ansible thing, we are going to add all the other
# nodes to the known-hosts
- name: run ssh-keyscan for hostnames on ceph_user
  shell: "ssh-keyscan -H {{ item.hostname }} >> /home/{{ ceph_user }}/.ssh/known_hosts"
  args:
    chdir: "/home/{{ ceph_user }}"
  become: yes
  with_items: "{{ cluster }}"

- name: run ssh-keyscan for ips on ceph_user
  shell: "ssh-keyscan -H {{ item.ip }} >> /home/{{ ceph_user }}/.ssh/known_hosts"
  args:
    chdir: "/home/{{ ceph_user }}"
  become: yes
  with_items: "{{ cluster }}"

- name: Recursively change ownership of ceph_user home
  file:
    path: "/home/{{ ceph_user }}"
    state: directory
    recurse: yes
    owner: "{{ ceph_user }}"
    group: "{{ ceph_user }}"
  become: yes

# enables root to access ceph as well
- name: add .ssh to root
  file:
    path: /root/.ssh/
    state: directory
  become: yes

- name: wipe known hosts
  copy:
    content: ""
    dest: /root/.ssh/known_hosts
    force: yes
    mode: 0644
  become: yes

- name: copy ssh config file to client (root)
  template:
    src: templates/common_ssh_config.j2
    dest: /root/.ssh/config
    mode: 0700
  become: yes

- name: run ssh-keyscan root hostnames
  shell: "ssh-keyscan -H {{ item.hostname }} >> /root/.ssh/known_hosts"
  become: yes
  with_items: "{{ cluster }}"

- name: run ssh-keyscan root ips
  shell: "ssh-keyscan -H {{ item.ip }} >> /root/.ssh/known_hosts"
  become: yes
  with_items: "{{ cluster }}"
