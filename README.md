
DOES:
* Deletes /var/lib/ceph & /etc/ceph
* Rewrites /etc/hosts files
-- TODO: instead of template, append to file
* Creates a wheel group, adds to sudoers, passwordless (across all nodes)
* Creates user ceph-deploy of wheel group (across all nodes)
* Deletes all data located on {{ devices }}
* Globally accessable key material
* adds known hosts for root user
* overwrite root's ssh config file

TODO:
-- ansible etcd role in mergetb/ansible

ASSUMES:
* database group in ansible, with atleast 3 nodes
* devices group in ansible, set of device for ceph to use
* node ansible group, that contains all nodes
* cannot run a single ceph service multiple times on a single host
